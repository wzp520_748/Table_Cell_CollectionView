//
//  SuperViewController.m
//  MatchMaker
//
//  Created by mac on 2017/7/6.
//  Copyright © 2017年 janice. All rights reserved.
//

#import "SuperViewController.h"

@interface SuperViewController ()
@end

@implementation SuperViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.view endEditing:YES];
    if (!self.isNeedHud) {

    }
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor = [UIColor whiteColor];
    UIBarButtonItem *item = [[UIBarButtonItem alloc]initWithImage:[[UIImage imageNamed:@"log_back"] imageWithRenderingMode:(UIImageRenderingModeAlwaysOriginal)] style:UIBarButtonItemStylePlain target:self action:@selector(backClick)];
    item.tintColor = Color_3;
    self.navigationItem.leftBarButtonItem = item;

    self.navigationController.interactivePopGestureRecognizer.delegate=(id)self;
 
    
    
    
}

- (void)backClick {
    [self.navigationController popViewControllerAnimated:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
}

//- (void)shareWeChatWithShareInformation:(NSDictionary *)shareDic withFromType:(UMSocialPlatformType)platformType {
//    //uid,header,nickname,textType,type，sex
//
//
//
//    if (![[UMSocialManager defaultManager] isInstall:UMSocialPlatformType_WechatSession]) {
//        [ProgressHUD showError:@"请下载微信后再分享"];
//        return;
//    }
//
//    NSString *uid       = [NSString stringWithFormat:@"%@", [shareDic objectForKey:@"uid"]];
//    NSString *sex       = [NSString stringWithFormat:@"%@", [shareDic objectForKey:@"sex"]];
//    NSString *type      = [NSString stringWithFormat:@"%@", [shareDic objectForKey:@"type"]];
//    NSString *header    = [NSString stringWithFormat:@"%@", [shareDic objectForKey:@"header"]];
//    NSString *nickname  = [NSString stringWithFormat:@"%@", [shareDic objectForKey:@"nickname"]];
//    NSString *textType  = [NSString stringWithFormat:@"%@", [shareDic objectForKey:@"textType"]];
//
//    //创建分享消息对象
//    UMSocialMessageObject *messageObject = [UMSocialMessageObject messageObject];
//    //创建网页内容对象
//
//    ;
////    UIImage* thumbURL =  header;
//
//    NSString *title;
//    if ([textType isEqualToString:@"1.4.1"]) {
//        title = [NSString stringWithFormat:@"%@改行当媒婆了，既好玩又赚钱，你也来看看吧！",nickname];
//    }else if ([textType isEqualToString:@"1.4.2"]) {
//        title = [NSString stringWithFormat:@"我是%@，我在全民媒婆找对象，你做我的媒婆吧！",nickname];
//    }else if ([textType isEqualToString:@"1.4.3"]) {
//        title = @"王建强、孙亮都转行做媒婆了，你也快快来试试吧？";
//    }else if ([textType isEqualToString:@"1.4.5"]) {
//        title = @"这里有个不错的单身哦，快来看看吧！";
//    }else if ([textType isEqualToString:@"1.4.6"]) {
//        title = [NSString stringWithFormat:@"%@身边有好多美女帅哥，快去认识一下吧！",nickname];
//    }else if ([textType isEqualToString:@"1.4.8"]) {
//        if ([sex isEqualToString:@"1"]) {
//            title = [NSString stringWithFormat:@"这个帅哥我喜欢，我想和TA认识，帮忙安排一下吧！"];
//        }else {
//            title = [NSString stringWithFormat:@"这个妹子我喜欢，我想和TA认识，帮忙安排一下吧！"];
//        }
//    }
//
//    UIImageView *imgView = [[UIImageView alloc] init];
//    [imgView sd_setImageWithURL:[NSURL URLWithString:header] completed:^(UIImage *image, NSError *error, EMSDImageCacheType cacheType, NSURL *imageURL) {
//
//        UIImage *img = [UIImage imageNamed:@"com_head"];
//        if (image) {
//            img = image;
//        }
//        UMShareWebpageObject *shareObject = [UMShareWebpageObject shareObjectWithTitle:title descr:@"全民争当媒婆，共享单身资源，为脱单助力！" thumImage:img];
//        //设置网页地址m,
//        shareObject.webpageUrl = [NSString stringWithFormat:@"%@%@/%@/%@",SHARE_ADDRESS,uid,type,[USER_D objectForKey:UID]];
//        //分享消息对象设置分享内容对象
//        messageObject.shareObject = shareObject;
//        //调用分享接口
//        [[UMSocialManager defaultManager] shareToPlatform:platformType messageObject:messageObject currentViewController:self completion:^(id data, NSError *error) {
//            if (error) {
//                UMSocialLogInfo(@"************Share fail with error %@*********",error);
//            }else{
//                if ([data isKindOfClass:[UMSocialShareResponse class]]) {
//                    UMSocialShareResponse *resp = data;
//                    //分享结果消息
//                    UMSocialLogInfo(@"response message is %@",resp.message);
//                    //第三方原始返回的数据
//                    UMSocialLogInfo(@"response originalResponse data is %@",resp.originalResponse);
//
//                }else{
//                    UMSocialLogInfo(@"response data is %@",data);
//                }
//            }
//        }];
//    }];
//}

-(void)showNoDataView:(NSString *)text {
    if (_noDataView) {
        [_noDataView removeFromSuperview];
    }
    _noDataView = [[UIView alloc] init];
    UIImageView *imageV = [[UIImageView alloc] initWithFrame:CGRectMake((335*kScale-200*kScale)/2, 0, 200*kScale, 300*kScale)];
    imageV.image = [UIImage imageNamed:@"com_noData"];
    [_noDataView addSubview:imageV];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 335*kScale, 0)];
    [_noDataView addSubview:label];
    label.textColor = UIColorFromRGB(0x999999);
    label.font = Font_Medium(28*kScale);
    label.text = text;
    label.textAlignment = NSTextAlignmentCenter;
    label.numberOfLines = 0;
    [label sizeToFit];
    label.frame = CGRectMake(0, CGRectGetMaxY(imageV.frame)+25*kScale, 335*kScale, label.frame.size.height);
    _noDataView.bounds = CGRectMake(0, 0, 335*kScale, 325*kScale+label.frame.size.height);
    [self.view.subviews enumerateObjectsUsingBlock:^(UITableView* obj, NSUInteger idx, BOOL *stop) {
        if ([obj isKindOfClass:[UITableView class]]) {
            _noDataView.center = CGPointMake(obj.frame.size.width/2, obj.frame.size.height/2-30*kScale);
            [obj addSubview:_noDataView]; 
        }
    }];
}

-(void)removeNoDataImage {
    if (_noDataView) {
        [_noDataView removeFromSuperview];
        _noDataView = nil; 
    }
}
@end
