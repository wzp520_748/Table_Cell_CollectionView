//
//  HomeCell_CollectionView.m
//  MatchMaker
//
//  Created by mac on 2018/3/16.
//  Copyright © 2018年 janice. All rights reserved.
//

#import "HomeCell_CollectionView.h"
#import "HomesCell_CollectionViewCell.h"

@interface HomeCell_CollectionView()<UICollectionViewDelegate,UICollectionViewDataSource>
@end

@implementation HomeCell_CollectionView

- (UICollectionView *)collectionView {
    if (!_collectionView) {
        UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
        flowLayout.itemSize = CGSizeMake(210*kScale, 400*kScale);
        flowLayout.minimumLineSpacing = 20*kScale;
        flowLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, Screen_W, 400*kScale) collectionViewLayout:flowLayout];
        _collectionView.contentInset = UIEdgeInsetsMake(0, 15*kScale, 0, 15*kScale);
//        _collectionView.backgroundColor = [UIColor yellowColor];
        _collectionView.showsHorizontalScrollIndicator = NO;
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        [_collectionView registerClass:[HomesCell_CollectionViewCell class] forCellWithReuseIdentifier:@"HomesCell_CollectionViewCell"];
        [self.contentView addSubview:_collectionView];
    }
    return _collectionView;
}

+ (instancetype)cellWithTableView:(UITableView *)tableView{
    static NSString *identifier = @"HomeCell_CollectionView";
    HomeCell_CollectionView *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[HomeCell_CollectionView alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return cell;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(nullable NSString *)reuseIdentifier{
    if (self  = [super initWithStyle:style reuseIdentifier:reuseIdentifier]){
        [self collectionView];
    }
    return self;
}

- (void)setupRelaod:(NSArray *)array{
    [_collectionView reloadData];
}

#pragma mark - CollectionDelegate
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return 6;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    HomesCell_CollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"HomesCell_CollectionViewCell" forIndexPath:indexPath];
    cell.headImg.backgroundColor = [UIColor yellowColor];
    cell.nicknameLabel.text = @"独孤皇后或或";
    cell.descrip.text = @"讽德诵功还是多喝水是";
    return cell;
}

@end
