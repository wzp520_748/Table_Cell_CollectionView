//
//  SuperViewController.h
//  MatchMaker
//
//  Created by mac on 2017/7/6.
//  Copyright © 2017年 janice. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SuperViewController : UIViewController

@property (nonatomic,strong) UIView *noDataView;

@property (nonatomic, assign) BOOL isNoNeedsStyle;  // self.isNoNeedsStyle = YES; 在即将返回的界面 添加

@property (nonatomic,assign) BOOL isNeedHud;
- (void)backClick;
-(void)showNoDataView:(NSString *)text;
-(void)removeNoDataImage;
@end
