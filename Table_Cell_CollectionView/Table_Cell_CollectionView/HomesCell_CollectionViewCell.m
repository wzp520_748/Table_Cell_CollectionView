//
//  HomesCell_CollectionViewCell.m
//  MatchMaker
//
//  Created by mac on 2018/3/16.
//  Copyright © 2018年 janice. All rights reserved.
//

#import "HomesCell_CollectionViewCell.h"

@implementation HomesCell_CollectionViewCell
- (UIImageView *)headImg {
    if (!_headImg) {
        _headImg = [[UIImageView alloc] initWithFrame:CGRectMake(15*kScale, 39*kScale, 180*kScale, 180*kScale)];
        _headImg.contentMode = UIViewContentModeScaleAspectFill;
        _headImg.layer.cornerRadius = 8*kScale;
        _headImg.layer.masksToBounds = YES;
        _headImg.layer.borderColor = UIColorFromRGB(0xcccccc).CGColor;
        _headImg.layer.borderWidth = 1*kScale;
        [self.contentView addSubview:_headImg];
    }
    return _headImg;
}
- (UILabel *)nicknameLabel {
    if (!_nicknameLabel) {
        _nicknameLabel = [[UILabel alloc] initWithFrame:CGRectMake(15*kScale, CGRectGetMaxY(_headImg.frame)+15*kScale, 180*kScale, 35*kScale)];
        _nicknameLabel.font = Font_Bold(28*kScale);
        _nicknameLabel.textColor = UIColorFromRGB(0x111111);
        _nicknameLabel.textAlignment = NSTextAlignmentCenter;
        [self.contentView addSubview:_nicknameLabel];
        
        UIImageView *img = [[UIImageView alloc] init];
        img.tag = 1100;
        //        img.image = [UIImage imageNamed:@"mah_cer"];
        img.tintColor = UIColorFromRGB(0xff704f);
        img.image = [[UIImage imageNamed:@"mah_cer"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        img.contentMode = UIViewContentModeScaleAspectFit;
        [self.contentView addSubview:img];
    }
    return _nicknameLabel;
}
- (UILabel *)descrip {
    if (!_descrip) {
        _descrip = [[UILabel alloc] initWithFrame:CGRectMake(20*kScale, 298*kScale, 170*kScale, 80*kScale)];
        _descrip.numberOfLines = 2;
        _descrip.textColor = UIColorFromRGB(0x666666);
        _descrip.font = Font_Regular(24*kScale);
        [self.contentView addSubview:_descrip];
    }
    return _descrip;
}
@end
